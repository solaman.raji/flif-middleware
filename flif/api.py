import json
import logging
import os
import shutil
import urllib2
from uuid import UUID
import dicom

import requests
from django.core.files import File
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from middleware import settings
from .models import OrderModel, DicomModel, ReportModel
from .serializers import OrderSerializer, ReportSerializer

logger = logging.getLogger("middleware")


class OrderView(APIView):
    """
    this function will take params from the hub
    it has a secret key as url param for api validation
    params format:
    {
        order_guid: "String value" and not null,
        hospital_id: Integer and not null,
        doctor_id: Integer and excepts null,
        priority: "String value" and not null,
        provider_id: Integer and excepts null,
        is_storage: Boolean and not null
    }
    This set of data will save to the "order" table
    """
    def post(self, request):
        data = {}
        request_data = json.loads(request.body)
        logger.info("request data: {0}".format(request_data))

        secret_key = request.GET.get("secret_key")
        logger.info("secret_key: {0}".format(secret_key))

        if not secret_key or not str(secret_key) == settings.API_SECRET_KEY:
            err_msg = "Invalid API request"
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 401
            return Response(data)

        order_guid = request_data.get("order_guid")
        logger.info("order_guid in request data: {0}".format(order_guid))

        order_guid = validate_uuid4(order_guid)
        logger.info("order_guid after validation: {0}".format(order_guid))

        if not order_guid:
            err_msg = "Invalid order guid"
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 401
            return Response(data)

        provider_guid = request_data.get("provider_guid")
        logger.info("provider_guid in request data: {0}".format(provider_guid))

        provider_guid = validate_uuid4(provider_guid)
        logger.info("provider_guid after validation: {0}".format(provider_guid))

        if provider_guid is not False:
            request_data["provider_guid"] = provider_guid

        try:
            order, created = OrderModel.objects.update_or_create(
                order_guid=request_data["order_guid"],
                defaults=request_data,
            )
            logger.info("order object: {0}".format(order))
            logger.info("order object dict: {0}".format(order.__dict__))
            logger.info("order created: {0}".format(created))

            serializer = OrderSerializer(order)

            success_msg = "Order saved successfully"
            logger.info(success_msg)

            data["order"] = serializer.data
            data["success"] = success_msg
            data["status"] = 200
        except Exception as e:
            err_msg = "Unexpected error: {0}".format(e)
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 500

        return Response(data)


class DicomSyncView(APIView):
    """
    this function will take params from the hub
    it has a secret key as url param for api validation
    params format:
    {
        order_guid: "String value" and not null,
        file: File object
    }
    This set of data will save to the "dicoms" table
    """
    def post(self, request):
        data = {}
        secret_key = request.GET.get("secret_key")
        logger.info("secret_key: {0}".format(secret_key))

        if not secret_key or not str(secret_key) == settings.API_SECRET_KEY:
            err_msg = "Invalid API request"
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 401
            return Response(data)

        guid = request.GET.get("guid")
        jpegls_compressed = self._get_compressed_param(request)
        logger.info("Order guid in request: {} with jpegls_compressed: {}".format(guid, jpegls_compressed))

        guid = validate_uuid4(guid)
        logger.info("Order guid after validation: {0}".format(guid))

        if not guid:
            err_msg = "Invalid order guid"
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 401
            return Response(data)

        try:
            requested_file = request.FILES['file']
            logger.info("Requested file: {0}".format(requested_file))

            try:
                tmp_path = "/tmp/dicom-sync/"
                filepath = os.path.join(tmp_path, requested_file.name)
                logger.info("filepath: {0}".format(filepath))

                try:
                    with open(filepath, "wb+") as destination:
                        for chunk in requested_file.chunks():
                            destination.write(chunk)

                    self.process_compressed_tar(data, guid, tmp_path, requested_file.name, filepath,
                                                jpegls_compressed=jpegls_compressed)
                except Exception as e:
                    err_msg = "File cannot be uploaded. Reason: {0}".format(e)
                    logger.error(err_msg)
                    data["error"] = err_msg
                    data["status"] = 500
            except Exception as e:
                err_msg = "Cannot create the path. Reason: {0}".format(e)
                logger.error(err_msg)
                data["error"] = err_msg
                data["status"] = 500
        except OrderModel.DoesNotExist:
            err_msg = "Order {0} does not exist".format(guid)
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 400
        except Exception as e:
            err_msg = "Unexpected error: {0}".format(e)
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 500

        return Response(data)

    def _get_compressed_param(self, request):
        bool_values = {'TRUE': True, 'FALSE': False}
        compressed_param = request.GET.get("jpegls_compressed")
        if compressed_param and compressed_param.upper() in bool_values:
            return bool_values[compressed_param.upper()]
        else:
            return True

    def process_compressed_tar(self, data, guid, tmp_root_path, filename, filepath, **kwargs):
        jpegls_compressed = kwargs.pop('jpegls_compressed', True)
        logger.info("Process compressed dicom file {} in {}".format(filename, tmp_root_path))
        logger.info('DCM file is jpegls compressed: {}'.format(jpegls_compressed))
        order = OrderModel.objects.get(order_guid=guid)

        _unzip_dir = os.path.join(tmp_root_path, str(guid))
        logger.info("Unzip directory: {0}".format(_unzip_dir))

        os.makedirs(_unzip_dir)
        logger.info('Unzip directory created')

        decompress = os.system("cd " + str(tmp_root_path) + "; tar xvzf " + str(filename) + " -C " + str(guid))
        logger.info("Decompression process done. exit status: {}".format(decompress))

        order.unzip_status = 1

        if decompress is 0:
            os.remove(filepath)
            logger.info('List of files in unzip dir {}'.format(_unzip_dir))
            for file in os.listdir(_unzip_dir):
                logger.info(file)
            else:
                logger.info('End of filelist from unzip dir {}'.format(_unzip_dir))

            try:
                create_order_status = self.create_order(order)
                logger.info("Create order status: {0}".format(create_order_status))

                if create_order_status == 200:
                    dicom_decompress_dir = os.path.join(tmp_root_path, str(guid) + "-decompressed")
                    logger.info("decompressed_dir: {0}".format(dicom_decompress_dir))

                    os.makedirs(dicom_decompress_dir)
                    logger.info('Decompressed directory created')

                    for dicom_file in os.listdir(_unzip_dir):
                        logger.info("dicom_file: {0}".format(dicom_file))

                        dcm_file = DicomModel()
                        dcm_file.order = order

                        compressed_file_name = os.path.join(_unzip_dir, dicom_file)
                        logger.info("Compressed file path: {0}".format(compressed_file_name))

                        decompress_file_name = os.path.join(dicom_decompress_dir, dicom_file)
                        logger.info("Decompressed file path: {0}".format(decompress_file_name))

                        dicom_decompress_command = "dcmdjpls %s %s" % (compressed_file_name, decompress_file_name)
                        if jpegls_compressed:
                            decompress = os.system(dicom_decompress_command)
                            logger.info("Command {} exit status: {}".format(dicom_decompress_command, decompress))
                        else:
                            logger.info('jpegls_compressed: {} so Copy {} to {}'.format(
                                jpegls_compressed, compressed_file_name, decompress_file_name))
                            shutil.copyfile(compressed_file_name, decompress_file_name)

                        reopen = open(decompress_file_name, buffering=-1)
                        destination_file = File(reopen)
                        dcm_file.dicom_image = destination_file
                        dcm_file.save()

                        try:
                            send_dicoms_to_platform_status = self.send_dicoms_to_platform(order, dcm_file)
                            logger.info("Send dicoms to platform status: {0}".format(send_dicoms_to_platform_status))

                            if send_dicoms_to_platform_status == 200:
                                logger.info("Dicom {0} sent to platform".format(dcm_file.dicom_image.name))
                                dcm_file.sync_status = 1
                                dcm_file.save()

                            os.remove(decompress_file_name)
                            logger.info("Decompressed file deleted")

                            os.remove(compressed_file_name)
                            logger.info("Compressed file deleted")
                        except Exception as e:
                            logger.error("Cannot upload dicom image. Reason: {0}".format(e))

                    shutil.rmtree(dicom_decompress_dir, ignore_errors=True)
                    logger.info('Decompressed directory deleted')

                    if order.dicoms.filter(sync_status=0).count() == 0:
                        logger.info("All dicom files for order {0} successfully synced to platform".format(
                            order.order_guid
                        ))
                        activate_order_status = self.activate_order(order)
                        logger.info("Activate order status: {0}".format(activate_order_status))

                        if activate_order_status == 200:
                            order.save()
                            serializer = OrderSerializer(order)

                            success_msg = "Successfully uploaded all dicom files and activate order to platform"
                            logger.info(success_msg)

                            data["order"] = serializer.data
                            data["success"] = success_msg
                            data["status"] = 200

                            order_guid = validate_uuid4(order.order_guid)
                            logger.info("order_guid: {0}".format(order_guid))

                            if order_guid:
                                dicom_media_dir = os.path.join(settings.MEDIA_ROOT, 'dicoms', order_guid)
                                logger.info("dicom_media_dir: {0}".format(dicom_media_dir))

                                shutil.rmtree(dicom_media_dir, ignore_errors=True)
                                logger.info("Dicom media directory deleted")
                            else:
                                logger.error("Cannot delete dicom media directory")

                            logger.info('Deleting order {} with dicoms after it is synced to platform'.format(order_guid))
                            order.dicoms.all().delete()
                            order.delete()
                            logger.info('Deleted order {} after it is synced to platform'.format(order_guid))
                        else:
                            err_msg = "Cannot activate order in platform"
                            logger.error(err_msg)
                            data["error"] = err_msg
                            data["status"] = 500
            except Exception as e:
                err_msg = "Cannot create order. Reason: {0}".format(e)
                logger.error(err_msg)
                data["error"] = err_msg
                data["status"] = 500
        else:
            err_msg = "File cannot be decompressed"
            logger.error(err_msg)
            data["error"] = err_msg
            data["status"] = 500

        shutil.rmtree(_unzip_dir, ignore_errors=True)
        logger.info('Order temp directory deleted')

    def create_order(self, order):
        logger.info("Creating new order in platform")

        try:
            order_create_url = settings.AH_SERVER + '/api/orders/' + '?secret_key=' + settings.CLIENT_SECRET_KEY
            logger.info("Order create url: {0}".format(order_create_url))

            data = {
                'hospital_id': order.hospital_id,
                'priority': order.priority,
                'doctor_id': order.doctor_id,
                'provider_id': order.provider_id,
                'provider_guid': order.provider_guid and str(order.provider_guid),
                'is_storage': order.is_storage,
            }

            if order.ah_order_guid:
                data["ah_order_guid"] = str(order.ah_order_guid)

            logger.info("Order create request data: {0}".format(data))

            response = requests.post(order_create_url, data=json.dumps(data), headers={'content-type': 'text/plain'})
            logger.info("Order create response: {0}".format(response))
            logger.info("Order create response dict: {0}".format(response.__dict__))

            if response.status_code == 200:
                order.ah_order_id = response.json()["id"]
                order.ah_order_guid = response.json()["guid"]
                logger.info("Order with id {0} and guid {1} created on platform".format(
                    order.ah_order_id, order.ah_order_guid
                ))
                order.save()
                return 200
            else:
                logger.info("Cannot create order on platform")
                return 500
        except Exception as e:
            logger.info("Cannot create order in platform. Reason: {0}".format(e))
            return 500

    def send_dicoms_to_platform(self, order, dcm_file):
        logger.info("Sending dicoms to platform")
        register_openers()

        url = settings.AH_SERVER + '/api/dicoms/' + '?secret_key=' + settings.CLIENT_SECRET_KEY + '&guid=' + str(order.ah_order_guid)
        logger.info("Dicom sync url: {0}".format(url))

        try:
            dicom_path = settings.MEDIA_ROOT + "/" + str(dcm_file.dicom_image.name)
            logger.info("Dicom file path: {0}".format(dicom_path))
            logger.info("Sending dicom {0} to platform".format(dcm_file.dicom_image))

            datagen, headers = multipart_encode({"file": open(dicom_path, "rb")})

            try:
                request = urllib2.Request(url, datagen, headers)
                logger.info("Dicom sync request: {0}".format(request))
                logger.info("Dicom sync request dict: {0}".format(request.__dict__))

                response = urllib2.urlopen(request)
                logger.info("Dicom sync response: {0}".format(response))
                logger.info("Dicom sync response dict: {0}".format(response.__dict__))

                pydicom_file = dicom.read_file(dicom_path)

                logger.info('dcm file sending for ah order guid: {}, patient: {}, gender: {}, Telephone: {}'.format(
                    order.ah_order_guid,
                    str(pydicom_file.get('PatientName')),
                    str(pydicom_file.get('PatientSex')),
                    str(pydicom_file.get('PatientTelephoneNumbers'))))

                if response.getcode() == 200:
                    logger.info("Order {0} dicom {1} successfully sync to platform".format(
                        order.id,
                        dcm_file.id
                    ))
                    return 200
                else:
                    logger.error("Order {0} dicom {1} does not sync to platform".format(
                        order.id,
                        dcm_file.id
                    ))
                    return 500
            except Exception as e:
                logger.error("Cannot send dicom. Reason: {0}".format(e))
                return 500
        except Exception as e:
            logger.error("Cannot upload files. Reason: {0}".format(e))
            return 500

    def activate_order(self, order):
        logger.info("Activating order in platform")
        order_url = settings.AH_SERVER + '/api/orders/' + '?secret_key=' + settings.CLIENT_SECRET_KEY
        logger.info("Activate order url: {0}".format(order_url))

        data = {
            'ah_order_guid': order.ah_order_guid,
            'active_status': 1,
            'hospital_id': order.hospital_id
        }
        logger.info("Activate order request data: {0}".format(data))

        try:
            response = requests.post(order_url, data=json.dumps(data), headers={'content-type': 'text/plain'})
            logger.info("Activate order response: {0}".format(response))
            logger.info("Activate order response dict: {0}".format(response.__dict__))

            if response.status_code == 200:
                order.sync_status = 1
                order.save()
                logger.info("Order {0} successfully sync all the dicoms to platform".format(order.order_guid))
                return 200
            else:
                logger.error("Cannot update the status of order {0} in platform".format(order.order_guid))
                return 500
        except Exception as e:
            logger.error("Cannot sync all the dicoms for order {0}. Reason: ".format(order.order_guid, e))
            return 500


class FileView(APIView):
    parser_classes = (MultiPartParser, FormParser,)

    @staticmethod
    def post(request):
        up_file = request.FILES['file']
        file_path = os.path.join(settings.BASE_DIR, "media", "images", up_file.name)
        destination = open(file_path, 'wb+')

        # patient_id = request.POST['data']['patient_id']
        # patient_name = request.POST['data']['patient_name']
        # hospital_id = request.POST['data']['hospital_id']
        # hospital_name = request.POST['data']['hospital_name']

        info_data = request.POST['data']

        for chunk in up_file.chunks():
            destination.write(chunk)
        destination.close()

        upload_to_s3(up_file.name)

        data = {
            "file_name": up_file.name,
            "file_size": str(up_file.size) + " bytes",
            # "patient_id": patient_id,
            # "patient_name": patient_name,
            # "hospital_id": hospital_id,
            # "hospital_name": hospital_name,
            "info_data": info_data,
        }

        return Response(data)


def upload_to_s3(filename):
    import boto3
    from botocore.client import Config

    ACCESS_KEY_ID = 'AKIAI35L72NHQARVCQGA'
    ACCESS_SECRET_KEY = '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'
    BUCKET_NAME = 'orderticket-images'

    file_path = settings.BASE_DIR + '/media/images/' + filename
    data = open(file_path, 'rb')

    s3 = boto3.resource(
        's3',
        aws_access_key_id=ACCESS_KEY_ID,
        aws_secret_access_key=ACCESS_SECRET_KEY,
        config=Config(signature_version='s3v4')
    )

    s3.Bucket(BUCKET_NAME).put_object(Key=filename, Body=data)


class ReportView(APIView):
    def post(self, request):
        data = {}
        parser = json.loads(request.body)

        try:
            report = ReportModel()

            if parser["procedure"]:
                report.procedure = parser["procedure"]
            if parser["clinical_information"]:
                report.clinical_information = parser["clinical_information"]
            if parser["comparison"]:
                report.comparison = parser["comparison"]
            if parser["findings"]:
                report.findings = parser["findings"]
            if parser["impression"]:
                report.impression = parser["impression"]

            report.report_name = parser["report_name"]
            report.provider_id = parser["provider_id"]

            report.save()
            serializer = ReportSerializer(report)

            data["report"] = serializer.data
            data["status"] = 200
        except Exception as e:
            data["error"] = "Cannot create report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def get(self, request):
        data = {}
        report_id = request.GET.get("report_id")
        provider_id = request.GET.get("provider_id")

        if report_id:
            try:
                report = ReportModel.objects.get(pk=report_id, provider_id=provider_id)
                serializer = ReportSerializer(report)
                data['report'] = serializer.data
                data["status"] = 200
            except ReportModel.DoesNotExist:
                data["error"] = "Cannot find report with id: " + str(report_id)
                data["status"] = 500
        else:
            try:
                page = request.GET.get('page')
                per_page = request.GET.get('per_page')

                if not per_page:
                    post_per_page = 25
                else:
                    post_per_page = int(per_page)

                if not page:
                    page = 1
                else:
                    page = int(page)

                offset = page * post_per_page - post_per_page
                pagesize = page * post_per_page
                reports = ReportModel.objects.filter(provider_id=provider_id).all()
                count = reports.count()

                if count > 0:
                    reports = reports[offset:pagesize]
                    serializer = ReportSerializer(reports, many=True)
                    data["reports"] = serializer.data
                    data["count"] = count
                    data["status"] = 200
                else:
                    data["error"] = "There is no report here."
                    data["status"] = 400
            except Exception as err:
                data["error"] = "Cannot retrieve data. Reason: " + str(err)
                data["status"] = 500

        return Response(data)

    def put(self, request):
        data = {}
        parser = json.loads(request.body)

        try:
            report = ReportModel.objects.get(pk=parser["report_id"], provider_id=parser["provider_id"])

            if parser["procedure"]:
                report.procedure = parser["procedure"]
            if parser["clinical_information"]:
                report.clinical_information = parser["clinical_information"]
            if parser["comparison"]:
                report.comparison = parser["comparison"]
            if parser["findings"]:
                report.findings = parser["findings"]
            if parser["impression"]:
                report.impression = parser["impression"]

            report_id = parser["report_id"]
            provider_id = parser["provider_id"]

            report.save()
            serializer = ReportSerializer(report)
            data["report"] = serializer.data
            data["status"] = 200
        except ReportModel.DoesNotExist:
            data["error"] = "Cannot find report with id: " + str(parser["report_id"])
            data["status"] = 400
        except Exception as e:
            data["error"] = "Cannot update report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def delete(self, request):
        data = {}
        report_id = request.GET.get("report_id")

        try:
            ReportModel.objects.filter(pk=report_id).delete()
            data["success"] = "Successfully deleted the report"
            data["status"] = 200
        except Exception as err:
            data["error"] = "Cannot delete the report. Reason: " + str(err)
            data["status"] = 500

        return Response(data)


def validate_uuid4(uuid_string):
    try:
        guid = UUID(uuid_string, version=4).hex
        return guid
    except ValueError:
        return False
    except TypeError:
        return False

