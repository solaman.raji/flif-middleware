from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from flif.api import OrderView, DicomSyncView, ReportView, FileView
from transfer_api import FileUploadView, DeliveryView

urlpatterns = [
    # end point for the syncing and sending dicoms and creating orders to platform
    url(r'^api/order/?$', OrderView.as_view()),
    url(r'^api/dicoms/?$', DicomSyncView.as_view()),
    url(r'^api/report/?$', ReportView.as_view()),
    url(r'^api/file_upload/$', FileView.as_view()),
    url(r'^transfer/upload/(?P<guid>.+)/parts/(?P<partno>.+)?$', FileUploadView.as_view()),
    url(r'^transfer/delivery/(?P<guid>.+)?$', DeliveryView.as_view()),
]
# add the suffix pattern that will append to the main url list
urlpatterns = format_suffix_patterns(urlpatterns)
