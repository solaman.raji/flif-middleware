from rest_framework.serializers import ModelSerializer
from flif.models import OrderModel, ReportModel


class OrderSerializer(ModelSerializer):
    class Meta:
        model = OrderModel
        fields = '__all__'


class ReportSerializer(ModelSerializer):
    class Meta:
        model = ReportModel
        fields = '__all__'
