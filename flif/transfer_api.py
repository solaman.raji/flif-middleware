import logging
import os
import urllib2
import urlparse
import shutil

from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
from rest_framework.response import Response
from rest_framework.views import APIView


logger = logging.getLogger("middleware")
tmp_upload_dir = '/tmp/upload'


class FileUploadView(APIView):

    def get(self, request):
        logger.info("Request: %s" % request)

    # {u'filepart_name': [u'orders06'], u'details': [<InMemoryUploadedFile: file (application/zip)>], u'filename': [u'dicom-12222.zip']}>
    def post(self, request, guid, partno):
        logger.info("Store file parts: %s (%s)" % (guid, partno))
        logger.info("Request POST: %s" % request.POST)
        logger.info("Request data: %s" % request.data)
        secret_key = request.GET.get("secret_key")
        logger.info('Secret key: %s' % secret_key)
        assert secret_key


        # Request POST: <QueryDict: {u'filepart_name': [u'orders13'], u'filename': [u'dicom-12222.zip']}>
        filepart_name = request.POST['filepart_name']
        uploading_file = request.POST['source_file_name'] if 'source_file_name' in request.POST else 'Not found'
        logger.info("Uploading file is: " + str(uploading_file))
        logger.info("Uploading file part is: " + str(filepart_name))
        try:
            fileparts_storage_path = os.path.join(tmp_upload_dir, str(guid), 'parts')
            self._store_file(fileparts_storage_path, partno, request.data['filepart'])
        except IOError as ex:
            logger.error("Could not store file: %s" % ex.message)
        result = "Part %s of %s is received " % (filepart_name, uploading_file)
        logger.info(result)
        return Response(result)

    def _store_file(self, dest_dir, filepart, file):
        try:
            os.path.exists(dest_dir) or os.makedirs(dest_dir)
        except os.error as ex:
            logger.warning('OSError occured while creating directory: %s', ex.message)

        with open(os.path.join(dest_dir, str(filepart)), "wb+") as destination:
            for chunk in file.chunks():
                destination.write(chunk)


class DeliveryView(APIView):

    def check_secret_key(self, request):
        secret_key = request.GET.get("secret_key")
        logger.info('Secret key: %s' % secret_key)
        # assert secret_key

    def get(self, request, guid):
        logger.info("GET status of uploaded file: %s" % guid)
        self.check_secret_key(request)
        upload_status = self.get_fileparts_status(os.path.join(tmp_upload_dir, str(guid), 'parts'))
        return Response(data=upload_status, status=200, content_type='application/json')

    def get_fileparts_status(self, path):
        status = []
        logger.info('List files in path: %s' % path)
        for file in os.listdir(path):
            if file.endswith('zip'):
                continue
            filestat = os.stat(os.path.join(path, file))
            logger.info('Status of file %s: %s' %(file, filestat))
            status.append({'name': file, 'size': filestat.st_size})
        return status

    def post(self, request, guid):
        assert request.data['file']
        assert request.data['filename']
        assert request.data['destination-url']
        assert request.data['content-type']
        assert request.data['file-hash']
        assert request.data['file-size']
        logger.info("Request.data: %s" % request.data)
        logger.info("Request.POST: %s" % request.POST)

        destination_url = request.data['destination-url']
        logger.info('Transfer file {} to location: {}'.format(request.data['filename'], destination_url))

        merged_file = self.merge_fileparts(guid, request.data['filename'])
        self.check_filehash(merged_file, request.data['file-hash'])
        self.check_filesize(merged_file, request.data['file-size'])

        # FIXME: Failed to make this POST work with nginx and django Bad Gateway/Timeout error occurred
        # response = self.deliver_file(destination_url, request.data['filename'], merged_file, request.data['content-type'])
        # return Response(**response)

        order_guid = self.extract_order_guid(destination_url)
        jpegls_compressed = self.parse_jpegls_compressed(destination_url)
        response = self.process_file(order_guid, merged_file, jpegls_compressed=jpegls_compressed)
        logger.info("Process status: %s" % response)
        shutil.rmtree(self.get_upload_root(guid), ignore_errors=False)
        return Response(response)

    def process_file(self, guid, filepath, jpegls_compressed=True):
        path = "/tmp/dicom-delivery/"
        result = {}
        from flif.api import DicomSyncView
        DicomSyncView().process_compressed_tar(result, guid, path, filepath, filepath,
                                               jpegls_compressed=jpegls_compressed)
        return result

    def deliver_file(self, url, filename, filepath, content_type):
        logger.info('Deliver file %s to %s' % (filename, url))
        dest_url = self.replace_remote_host(url)
        logger.info('New URL to post file: %s' % dest_url)

        response = self.post_file(dest_url, filepath)
        if response['status_code'] == 200:
            result = "File transfer successful"
            logger.info(result)
            return {'message': result, 'status_code': 200}
        else:
            failed = "File Transfer failed"
            logger.info(failed)
            return {'message': failed, 'status_code': 500}

    def get_fileparts(self, guid):
        fileparts_root = self.get_fileparts_root(guid)
        paths = [os.path.join(fileparts_root, file) for file in os.listdir(fileparts_root)]
        paths = sorted(paths)
        return paths

    def get_fileparts_root(self, guid):
        return os.path.join(self.get_upload_root(guid), 'parts')

    def get_upload_root(self, guid):
        return os.path.join(tmp_upload_dir, str(guid))

    def get_merged_path(self, guid, filename):
        return os.path.join(self.get_upload_root(guid), filename)

    def merge_fileparts(self, guid, filename):
        merged_file_path = self.get_merged_path(guid, filename)
        logger.info('Merge file parts into %s' % merged_file_path)
        with open(merged_file_path, 'wb+') as gzip:
            for filepath in self.get_fileparts(guid):
                logger.info('Merge part: %s' % filepath)
                with open(filepath) as f:
                    gzip.write(f.read())
        return merged_file_path

    def check_filehash(self, file_path, file_hash):
        pass

    def check_filesize(self, file_path, file_size):
        pass

    def replace_remote_host(self, url):
        url_parts = urlparse.urlparse(url)
        return url_parts._replace(netloc='localhost').geturl()

    def generate_host_header(self, url):
        url_parts = urlparse.urlparse(url)
        return {'Host': url_parts.netloc}

    # FIXME: Implemented as hack. Request to nginx again in same session was not working
    def extract_order_guid(self, url):
        url_parts = urlparse.urlparse(url)
        query = urlparse.parse_qs(url_parts.query)
        return query['guid'][0]

    @classmethod
    def parse_jpegls_compressed(cls, url, default=True):
        try:
            url_parts = urlparse.urlparse(url)
            query = urlparse.parse_qs(url_parts.query)
            if 'jpegls_compressed' not in query:
                return default
            return query['jpegls_compressed'][0].lower() == 'true'
        except KeyError:
            return default
        except Exception:
            logger.exception('Exception occurred while parsing jpegls_compressed from url')
            return default

    def post_file(self, url, filepath):
        register_openers()
        datagen, headers = multipart_encode({"file": open(filepath, "rb")})
        logger.info("datagen: " + str(datagen))
        logger.info("headers: " + str(headers))

        request = urllib2.Request(url, datagen, headers)
        logger.info("Middleware dicom sync API request as dict: " + str(request.__dict__))
        response = urllib2.urlopen(request)

        logger.info("Middleware dicom sync API response as json: " + str(response))
        return {'status_code': response['status'], 'message': response['status']}
