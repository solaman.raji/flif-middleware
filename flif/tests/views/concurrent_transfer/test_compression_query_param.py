from django.test import TestCase, RequestFactory

from flif.api import DicomSyncView
from flif.transfer_api import DeliveryView


class CompressionQueryParamInUrl(TestCase):
    def test_positive_value_if_query_param_passed(self):
        assert DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer?jpegls_compressed=True')
        assert DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer?jpegls_compressed=true')

    def test_negative_value_if_query_param_passed(self):
        assert not DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer?jpegls_compressed=False')
        assert not DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer?jpegls_compressed=false')

    def test_default_value_if_param_absent(self):
        assert DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer?jpegls_compressed=')
        assert DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer?')
        assert DeliveryView.parse_jpegls_compressed('http://localhost:3000/transfer')


class CompressionQueryParamInRequest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()

    def test_positive_value_if_query_param_passed(self):
        request = self.factory.get('/api/dicoms/?jpegls_compressed=True')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), True)

        request = self.factory.get('/api/dicoms/?jpegls_compressed=true')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), True)

    def test_negative_value_if_query_param_passed(self):
        request = self.factory.get('/api/dicoms/?jpegls_compressed=False')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), False)

        request = self.factory.get('/api/dicoms/?jpegls_compressed=false')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), False)

    def test_default_value_if_param_has_no_value(self):
        request = self.factory.get('/api/dicoms/?jpegls_compressed=')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), True)

        request = self.factory.get('/api/dicoms/?jpegls_compressed')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), True)

    def test_default_value_if_param_absent(self):
        request = self.factory.get('/api/dicoms/')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), True)

    def test_default_value_if_param_has_invalid_value(self):
        request = self.factory.get('/api/dicoms/?jpegls_compressed=falsy')
        self.assertEqual(DicomSyncView()._get_compressed_param(request), True)
