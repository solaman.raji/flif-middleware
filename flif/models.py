from django.db import models
from uuidfield import UUIDField


def dicom_file_upload_dir(instance, filename):
    t = filename.split(" ")

    # there is space in filename need to replace with '_'
    if len(t) > 1:
        filename = filename.replace(" ", "_")

    # if it is using the tmp dir for zpaq compression
    if filename.find('tmp') >= 0:
        file_array = filename.split('/')
        return 'dicoms/' + str(instance.order.order_guid) + '/' + file_array[3]
    else:
        return 'dicoms/' + str(instance.order.order_guid) + '/' + filename


class OrderModel(models.Model):
    order_guid = models.CharField(max_length=250, unique=True)
    hospital_id = models.IntegerField()
    priority = models.CharField(max_length=150)
    doctor_id = models.IntegerField(null=True, blank=True)
    provider_id = models.IntegerField(null=True, blank=True)
    provider_guid = UUIDField(null=True, blank=True)
    is_storage = models.BooleanField()
    ah_order_guid = models.CharField(max_length=250, blank=True, null=True)
    ah_order_id = models.IntegerField(blank=True, null=True)
    unzip_status = models.IntegerField(default=0)
    sync_status = models.IntegerField(default=0)

    def __unicode__(self):
        return str(self.order_guid)

    class Meta:
        db_table = "order"


class DicomModel(models.Model):
    dicom_image = models.FileField(upload_to=dicom_file_upload_dir, null=True, blank=True)
    order = models.ForeignKey(OrderModel, blank=True, null=True, related_name="dicoms")
    sync_status = models.IntegerField(default=0)

    def __unicode__(self):
        return str(self.dicom_image)

    class Meta:
        db_table = "dicoms"


class ReportModel(models.Model):
    procedure = models.TextField()
    clinical_information = models.TextField()
    comparison = models.TextField()
    findings = models.TextField()
    impression = models.TextField()
    report_name = models.CharField(max_length=200)
    provider_id = models.IntegerField()

    def __unicode__(self):
        return str(self.report_name)

    class Meta:
        db_table = "report"
