from fabric.context_managers import cd, prefix
from fabric.contrib.files import exists
from fabric.contrib.project import rsync_project
from fabric.operations import *

from middleware import settings as flif_settings

abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)


# --------------------------------------------
# Flif-middleware platform cofiguration
# --------------------------------------------

class FabricException(Exception):
    pass


# fab mid deploy install

def mid(target_host=None, env_type='dev'):
    assert env_type in ['prod', 'dev', 'azure']
    print "Connecting to flif-middleware"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.warn_only = False
    env.password = 'ubuntu'
    env.abort_exception = FabricException

    env.env_type = env_type
    env.hosts = [
        target_host,
    ]

    env.graceful = False
    env.is_grunt = True
    env.user = 'ubuntu'
    env.home = '/home/%s' % env.user
    env.project = 'middleware'

    # local config file path
    env.serverpath = '%s/%s' % (env.home, env.project)
    env.virtualenv_home = '%s/virtualenv' % env.home
    env.virtualenvpath = '%s/%s' % (env.virtualenv_home, env.project)

    env.nginx_config = 'devops/nginx.conf'
    env.src_settings_root = 'devops/settings'
    env.supervisor_file_path = 'devops/supervisor'
    env.supervisor_config_path = '/etc/supervisor/conf.d'
    env.uwsgi_config = '%s/uwsgi.conf' % (env.supervisor_file_path)
    env.tmp_config = 'devops/tmp.alemhealth.conf'

    env.media_path = '%s/media' % (env.serverpath)

    env.rsync_exclude = [
        '*.sqlite3',
        '*/.DS_Store*',
        '*/.readme.md.*',
        'media/*',
        '*.pyc',
        '*.pem',
        '*/migrations',
        'middleware/local_settings.py',
        'fab*',
        'devops'
    ]

    return


def install():
    update()
    install_python_dependency()
    install_pip()
    install_virtualenv()
    install_dcmtk()
    install_nginx()
    config_nginx()
    create_directories()
    install_supervisor()
    config_supervisor()
    # dir_permission()
    restart_supervisor()
    return


def create_directories():
    var_dirs = '/var/{log,cache,lib,backups,www}/alemhealth'
    sudo('mkdir -vp %s' % var_dirs)
    sudo('chown -R %s:%s %s' % (env.user, env.user, var_dirs))


def install_python_dependency():
    print "Installing python dependency packages"
    sudo('apt-get install build-essential libssl-dev libffi-dev python-dev -y')
    sudo(
        'apt-get -y install '
        'python-setuptools '
        'python-dev make automake gcc '
        'libxml2 libxml2-dev libxslt-dev '
        'libtidy-dev python-lxml '
        'htop iftop'
    )
    sudo(
        'apt-get install -y '
        'libjpeg62 zlib1g-dev libjpeg8-dev '
        'libjpeg-dev libfreetype6-dev '
        'libpng-dev libgif-dev'
    )
    sudo('apt-get -y install libcurl4-gnutls-dev librtmp-dev')
    sudo('apt-get install -y libjpeg-turbo8-dev')
    sudo('apt-get install -y libjpeg62-dev')


def install_pip():
    print "Installing python pip in the system"
    sudo('curl -O https://bootstrap.pypa.io/get-pip.py')
    sudo('python get-pip.py')
    sudo('rm get-pip.py')
    sudo('pip install -U pip')


def install_virtualenv():
    print "Installing python virtualenv"
    sudo('pip install virtualenv virtualenvwrapper')
    run('mkdir -p %s' % env.virtualenvpath)
    print "Creating flif-middleware virtualenv"
    run('cd %s; virtualenv %s' % (env.virtualenv_home, env.project))
    # sudo('chown -R ubuntu:root %s/virtualenv' % env.home)
    print "Virtualenv installed"


def install_pip_requirements():
    print "Installing pip requirements"
    with cd(env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        run('pip install -r requirements.txt')
        run('pip install uwsgi')


def install_dcmtk():
    print 'Installing dcmtk'
    sudo('apt-get install -y dcmtk')


def install_nginx():
    print 'Installing NGINX'
    sudo("apt-get install -y nginx")
    sudo('systemctl enable nginx')
    default_config = '/etc/nginx/sites-enabled/default'
    if exists(default_config):
        sudo('rm %s' % (default_config))
        print 'Deleted NGINX default config'
    sudo('systemctl enable nginx')
    sudo('systemctl start nginx')


def config_nginx():
    print 'Configuring NGINX'
    default_config = '/etc/nginx/sites-enabled/nginx.conf'
    if exists(default_config):
        sudo('rm /etc/nginx/sites-enabled/nginx.conf')
        print 'Deleted NGINX default config'

    print 'Install NGINX config'

    put('%s/%s' % (flif_settings.BASE_DIR, env.nginx_config), '/etc/nginx/sites-available/flif', use_sudo=True)
    sudo('ln -fs %s %s' % ('/etc/nginx/sites-available/flif', '/etc/nginx/sites-enabled/flif'))
    sudo('nginx -t')
    sudo('systemctl restart nginx')


def install_supervisor():
    print 'Installing Supervisor'
    sudo('apt-get -y install supervisor')


def config_supervisor():
    print 'Configuring supervisor config files'
    print 'Store uwsgi supervisor config script'
    put(env.uwsgi_config, "%s/%s" % (env.supervisor_config_path, "uwsgi.conf"), use_sudo=True)
    sudo('chown root:root %s/*.conf' % (env.supervisor_config_path))

    print 'Copy tmp file config'
    put(env.tmp_config, '/usr/lib/tmpfiles.d/', use_sudo=True)


def restart_supervisor():
    print 'Restarting supervisor configurations'
    sudo('supervisorctl stop all', shell=False)
    sudo('supervisorctl reread all', shell=False)
    sudo('supervisorctl update all', shell=False)
    sudo('supervisorctl start all', shell=False)


# def dir_permission():
#     print "setting permission to tmp and media directory"
#     sudo('chown -R %s:%s /tmp/' % (env.user, env.user))
#     sudo('chmod -R u+x /tmp/')
#     sudo('chown -R %s:%s %s/' % (env.user, env.user, env.media_path))
#     sudo('chmod -R u+x %s/' % (env.media_path))


def update():
    print 'Start updating the system'
    sudo('apt-get update')


def upgrade():
    print 'Start upgrading the system'
    sudo('apt-get upgrade')


# --------------------------------------------
# Deploying Flif-middleware
# --------------------------------------------

def deploy():
    print 'Starting to deploy flif-middleware'
    sync_code_base()
    install_pip_requirements()
    with cd('%s' % env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        print 'Running migrations'
        run(' ./manage.py makemigrations')
        run(' ./manage.py migrate')
        run(' ./manage.py collectstatic --noinput')
    default_config = '/usr/bin/supervisorctl'
    copy_settings()
    if exists(default_config):
        restart_supervisor()
    print 'Finished the deployment of flif-middleware'
    return


def sync_code_base():
    print 'Syncing flif-middleware code base'
    rsync_project(env.serverpath, abspath('') + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')


def copy_settings():
    src_file = env.env_type + '.local_settings.py'
    config_src_path = os.path.join(env.src_settings_root, src_file)
    config_dest_path = os.path.join(env.serverpath, 'middleware', 'local_settings.py')
    if not exists(config_dest_path, verbose=True):
        print 'Copying %s as config to %s' % (config_src_path, config_dest_path)
        put(config_src_path, config_dest_path, use_sudo=False)
